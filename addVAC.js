(function() {

    function htmlToElement(html) {
      var template = document.createElement('template');
      html = html.trim();
      template.innerHTML = html;
      return template.content.firstChild;
    }
    function insertAfter(el, referenceNode) {
      referenceNode.parentNode.insertBefore(el, referenceNode.nextSibling);
    }
    function manageExistingBan(el) {
      var hasVACBan = false;
      [].forEach.call(el.getElementsByClassName('profile_ban'), ban => {
        if (ban.innerText.includes('VAC ban on record')) {
          ban.firstChild.textContent = 'Multiple VAC bans on record ';
          hasVACBan = true;
          return;
        } else if (ban.innerText.includes('Multiple VAC bans on record'))
          hasVACBan = true;
      });
      if (!hasVACBan) {
        var VACBan = htmlToElement(
            '<div class="profile_ban">1 VAC ban on record <span class="profile_ban_info">| <a class="whiteLink" href="https://support.steampowered.com/kb_article.php?ref=7849-Radz-6869&amp;l=english" target="_blank" rel="noreferrer">Info</a></span>');
        el.prepend(VACBan);
      }
      el.lastChild.textContent = '0 day(s) since last ban';
    }
    var base = document.body.getElementsByClassName('profile_rightcol')[0];
    if (base.getElementsByClassName('profile_ban_status')[0]) return manageExistingBan(base.getElementsByClassName('profile_ban_status')[0]);
    var VACBan = htmlToElement(
        '<div class="profile_ban_status"><div class="profile_ban">1 VAC ban on record <span class="profile_ban_info">| <a class="whiteLink" href="https://support.steampowered.com/kb_article.php?ref=7849-Radz-6869&amp;l=english" target="_blank" rel="noreferrer">Info</a></span></div>0 day(s) since last ban</div>');
    if (!base.firstElementChild)
      base.appendChild(VACBan);
    else
      insertAfter(VACBan, base.firstElementChild);
})();